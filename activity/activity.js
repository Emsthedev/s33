//3
async function fetchTodos() {
    let result = await fetch("https://jsonplaceholder.typicode.com/todos");
    let response = await result.json();
    console.log(response);
}
fetchTodos();

// 4
async function fetchTitles() {
    const result = await fetch("https://jsonplaceholder.typicode.com/todos");
    const response = await result.json();
    const showTitles = response.map((json) => `title: ${json.title}`);
    console.log(showTitles);
}
fetchTitles();

// 5
fetch("https://jsonplaceholder.typicode.com/todos/99", {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then((res) => res.json())
    .then((data) => console.log(data))


//6
fetch("https://jsonplaceholder.typicode.com/todos/99", {
        method: "GET",
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then((res) => res.json())
    .then((data) => {
        console.log("title: " + data.title);
        console.log("completed: " + data.completed)
    });

//7
async function newTodo() {
    const result = await fetch("https://jsonplaceholder.typicode.com/todos", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            userId: 7,
            completed: true,
            title: "Created Post"
        })
    });

    const response = await result.json();
    console.log(response);
}
newTodo();

//8
async function updateOneTodo() {
    const result = await fetch("https://jsonplaceholder.typicode.com/todos/99", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "sample title updated"
        })
    });

    const response = await result.json();
    console.log(response);
}
updateOneTodo();

//9
async function updateContent() {
    const result = await fetch("https://jsonplaceholder.typicode.com/todos/99", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "modified title",
            description: "modified description",
            status: "completed",
            dateCompleted: '2022-08-27',
            userId: 101
        })
    });

    const response = await result.json();
    console.log(response);
}
updateContent();

//10
async function updatePatch() {
    const result = await fetch("https://jsonplaceholder.typicode.com/todos/98", {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "this is a modified todo",
            description: "modified description"
        })
    });

    const response = await result.json();
    console.log(response);
}
updatePatch();

//11
async function patchStatusDate() {
    const result = await fetch("https://jsonplaceholder.typicode.com/todos/99", {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            completed: true,
            dateCompleted: '2022-08-27'
        })
    });

    const response = await result.json();
    console.log(response);
}
patchStatusDate();

//12
async function deleteOne() {
    const result = await fetch("https://jsonplaceholder.typicode.com/todos/99", {
        method: "DELETE"
    });

    const response = await result.json();
    console.log(response);
}
deleteOne();