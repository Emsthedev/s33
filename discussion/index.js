//console.log("hello");
/* 
Javascript Synchronous and Asynchronous

JS is by defaul synchronous,it means that only one statement is executed at a time


*/
/* Synchronous */
/* console.log("Hello Ems");
consol.log("Hello Ems Again");
console.log("Bye Ems "); */

/* simple iteration */
/* console.log("Hello Ems");
for (let i = 0; i <= 1000; i++) {

    console.log(i);
}
console.log("Hello Ems again"); */

/* Asynchronous means that we can proceed to execute other statements/codeblock
while time consuming code is running in the background. */


/* Getting All Posts
    The Fetch API allows you to asynchronously request for a
    resources(data) from the server 
    
    A "promise" is an object that represents the eventual completion (or failure) of
    an asynchronous function and it's resulting value.*/

    /* 
    Syntax: 
    fetch("URL");
    */
   /* a promise maybe in one of the 3 possible states:
   -fulfilled
   -rejected
   -pending
   */
   console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

   /* Syntax: 
   fetch("URL")
   .then((response) => {//lines of codes}) */
    fetch("https://jsonplaceholder.typicode.com/posts")
    // by using the response.status we only check if the promiseis fulfilled or rejected
  /*   .then " method captures the response object and returns 
    another "promise"which will be evantaually be resolved or rejected
    */
    //.then((response)=>console.log(response.status))


    /* Use the "json" method from the response onject to convert the 
    data retrieved into JSON format
    to be use in our application */
   .then((response)=> response.json())
    /* Using multiple ".then" methods that creates a "promise chain" */
   // .then((json)=> console.log(json));

/* it Displays each title of the posts */
   /* .then((json)=> {
    json.forEach(posts => console.log(posts.title))
   }); */

   /* Asynchronous */
   async function fetchData() {

    /* await - it waits for the fetch methods to complete
    then it stores the value in the result variable */
    let result = await fetch("https://jsonplaceholder.typicode.com/posts")

    /* result returned by fetched is a promise return */
    console.log(result);
    /* A returned response is an object */
    console.log(typeof result);
    /* We cannot access the content of the "response" by directly accessing
    its body property */
    console.log(result.body);
    /* it converts the data from the "response" object to
    JSON format.  */
    let json = await result.json()
    console.log(json);
   }

   fetchData();

   // Getting a specific postman
   /* (retrieve, /posts/:id, GET) */
console.log("");
/* ":id" is wildcard where you can put the 
unique identifier as value to show specific data/resources. */
   fetch("https://jsonplaceholder.typicode.com/posts/10")
   .then((response) => response.json())
   .then((json) => console.log(json))

   //Creating a Post
   /*
            Syntax:
            fetch("URL", options)
            .then((response) => {line of codes}) 
            .then((data or json) => console.log() or {line of codes})
    */


 fetch("https://jsonplaceholder.typicode.com/posts", 
    {
        //Request
    //HTTP Method
    method:"POST", 
    // specifies the content that it will pass inJSON format
    headers: {
    "Content-Type" : "application/json"
            },
     //Sets the content/body data of the "Request" object to be sent
     //to the backend /server
    body:JSON.stringify({
       
        title : "New Post",
        body : "This is a new post, hello batch 197",
        userId : 1

                        })
    })
    //response of the server based on the request
.then((response)=> response.json())
.then((json) => console.log(json))


//Updating post (Put)
/* 
    (update, /posts/:id, PUT)
*/
console.log("");
fetch("https://jsonplaceholder.typicode.com/posts/10", 
{
    //PUT method is used to update the whole onject/documnet
    method: "PUT",
    headers:{
        "Content-Type" : "application/json"
    },
    body:JSON.stringify({
       
        title : "Updated Post",
        body : "This is a updated post, hello batch 197",
        userId : 1

                        })

})
.then((response)=> response.json())
.then((json) => console.log(json))

//Patch

fetch("https://jsonplaceholder.typicode.com/posts/10", 
{
    //PUT method is used to update the whole onject/documnet
    method: "PATCH",
    headers:{
        "Content-Type" : "application/json"
    },
    body:JSON.stringify({
       
        title : "Updated Patch Post (Corrected post)"

                        })

})
.then((response)=> response.json())
.then((json) => console.log(json))

//new
/* fetch("https://jsonplaceholder.typicode.com/posts/10", 
{
    //PUT method is used to update the whole onject/documnet
    method: "PATCH",
    headers:{
        "Content-Type" : "application/json"
    },
    body:JSON.stringify({
       
        title : request.body.title

                        })

})
.then((response)=> response.json())
.then((json) => console.log(json)) */


//DELETE a post
/* 
(delete, /posts/:id, DELETE)
*/
fetch("https://jsonplaceholder.typicode.com/posts/10", 
{
    //DELETE method is used to update the whole onject/documnet
    method: "DELETE"
   

})
.then((response)=> response.json())
.then((json) => console.log(json))